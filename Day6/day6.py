def day6(file):
    with open(file, "r") as f:
        string = f.read()
    races = list(zip(*map(lambda line: [int(n) for n in line.split(" ") if n.isnumeric()], string.split("\n"))))
    race = tuple(map(lambda line: int(line[line.index(":") + 1:].replace(" ", "")), string.split("\n")))
    races.append(race)

    ways = [0 for race in races]
    for i, race in enumerate(races):
        race_time, race_record = race

        for j in range(race_time):
            distance_travelled = j * (race_time - j)

            if distance_travelled > race_record:
                ways[i] = (race_time - (j * 2)) + 1
                break
    
    product = 1
    for n in ways[:-1]:
        product *= n

    print(f"Part 1: {product}")
    print(f"Part 2: {ways[-1]}")

day6("Day6/day6.txt")
