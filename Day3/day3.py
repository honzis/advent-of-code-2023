def day3(file):
    grid = []
    with open(file, "r") as f:
        for line in f:
            grid.append(line[:-1])
    
    part_numbers = []
    gear_ratios = []
    gears = {}

    for i, row in enumerate(grid):
        symbol_found = False
        gear_found = False
        number = ""
        for j, char in enumerate(row):
            if char.isnumeric():
                number += char
                if not symbol_found or not gear_found:
                    for k in range(i - 1, i + 2):
                        for l in range(j - 1, j + 2):
                            if k < 0 or l < 0 or k >= len(grid) or l >= len(row) or k == i and l == j:
                                continue
                            if not grid[k][l].isnumeric() and grid[k][l] != ".":
                                symbol_found = True
                                if grid[k][l] == "*":
                                    gear_found = True
                                    if (k, l) not in gears:
                                        gears[(k, l)] = []
                                    last_gear = (k, l)
                                    break
                        if symbol_found and gear_found:
                            break
                        
            if number.isnumeric() and not char.isnumeric() or j + 1 == len(row):
                if symbol_found:
                    part_numbers.append(int(number))
                    symbol_found = False
                if gear_found:
                    gears[last_gear].append(int(number))
                    gear_found = False
                number = ""
    
    for gear in gears.values():
        if len(gear) == 2:
            gear_ratios.append(gear[0] * gear[1])

    print(f"Part 1: {sum(part_numbers)}")
    print(f"Part 2: {sum(gear_ratios)}")

day3('Day3/day3.txt')