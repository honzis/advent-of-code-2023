def day5(file):
    seeds = []
    maps = []
    with open(file, "r") as f:
        for line in f:
            line = line.replace("\n", "")
            if line == "":
                continue
            elif "map" in line:
                maps.append([])
            elif "seeds" in line:
                seeds = tuple(map(int, line[7:].split(" ")))
            else:
                maps[-1].append(tuple(map(int, line.split(" "))))

    locations = []
    for seed in seeds:
        for i, map_ in enumerate(maps):
            for range_ in map_:
                d_range_start, s_range_start, range_length = range_
                if seed >= s_range_start and seed < s_range_start + range_length:
                    seed += d_range_start - s_range_start
                    break
            if i == len(maps) - 1:
                locations.append(seed)
    
    print(f"Part 1: {min(locations)}")

day5("Day5/day5.txt")