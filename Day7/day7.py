def day7(file):
    hands = []
    with open(file, "r") as f:
        for line in f:
            hand, bid = line.split(" ")
            bid = int(bid.replace("\n", ""))
            hands.append({"hand": hand, "bid": bid, "strength": 0})
    
    cards = tuple("AKQJT98765432")
    def get_strength(hand, cards):
        n1, n2 = tuple(sorted([hand.count(card) for card in cards], reverse=True))[:2]
        if n1 == 5:
            strength = 7
        elif n1 == 4:
            strength = 6
        elif n1 == 3 and n2 == 2:
            strength = 5
        elif n1 == 3:
            strength = 4
        elif n1 == 2 and n2 == 2:
            strength = 3
        elif n1 == 2:
            strength = 2
        else:
            strength = 1
        return strength

    def isstronger(hand1, hand2, cards):
        for i in range(5):
            index1, index2 = cards.index(hand1[i]), cards.index(hand2[i])
            if index1 < index2:
                return True
            elif index1 > index2:
                return False

    def sort_same_type(hands, cards):
        swapped = True
        while swapped:
            swapped = False
            for i in range(len(hands) - 1):
                hand1, hand2 = hands[i]["hand"], hands[i + 1]["hand"]
                if isstronger(hand1, hand2, cards):
                    hands[i], hands[i + 1] = hands[i + 1], hands[i]
                    swapped = True
        return hands

    for hand in hands:
        hand["strength"] = get_strength(hand["hand"], cards)
    hands = sorted(hands, key=lambda hand: hand["strength"])

    sorted_hands = []
    start = 0
    for i in range(len(hands)):
        if i == len(hands) - 1 or hands[i]["strength"] != hands[i + 1]["strength"]:
            sorted_hands += sort_same_type(hands[start:i + 1], cards)
            start = i + 1

    winnings = []
    for i, hand in enumerate(sorted_hands):
        winnings.append(hand["bid"] * (i + 1))
    print(f"Part 1: {sum(winnings)}")

day7("Day7/day7.txt")