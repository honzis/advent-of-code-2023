def day4(file):
    points = 0
    cards = 0
    with open(file, "r") as f:
        number_of_cards = [1 for line in f]
        f.seek(0)
        for i, line in enumerate(f):
            line = line[line.index(":") + 2:].replace("\n", "")
            split_numbers = lambda numbers: [int(n) for n in numbers.split(" ") if n.isnumeric()]
            winning_numbers, my_numbers = map(split_numbers, line.split("|"))

            matching_numbers = len([n for n in my_numbers if n in winning_numbers])
            card_points = 0
            for j in range(matching_numbers):
                number_of_cards[i + j + 1] += number_of_cards[i]
                if j == 0:
                    card_points = 1
                else:
                    card_points *= 2

            points += card_points
            cards += number_of_cards[i]
    
    print(f"Part 1: {points}")
    print(f"Part 2: {cards}")

day4("Day4/day4.txt")