def main(file):
    with open(file, "r") as f:
        histories = list(map(lambda l: list(map(int, l.split())), f.read().splitlines()))
    
    next_values = []
    prev_values = []
    for history in histories:
        sublists = [history]
        while not all([n == 0 for n in sublists[-1]]):
            sublists.append([])
            for i in range(len(sublists[-2]) - 1):
                sublists[-1].append(sublists[-2][i + 1] - sublists[-2][i])
        
        for i, l in enumerate(reversed(sublists)):
            if i == 0:
                l.append(0)
                l.insert(0, 0)
                continue
            l.insert(0, l[0] - sublists[-i][0])
            l.append(l[-1] + sublists[-i][-1])
        
        next_values.append(sublists[0][-1])
        prev_values.append(sublists[0][0])
    
    print(f"Part 1: {sum(next_values)}")
    print(f"Part 2: {sum(prev_values)}")

main("Day9/day9.txt")