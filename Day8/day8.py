import itertools
def day8(file):
    with open(file, "r") as f:
        directions, _, *nodes = f.read().splitlines()

    new_nodes = {}
    for n in nodes:
        node, elements = n.split(" = ")
        new_nodes[node] = elements[1:-1].split(", ")
    nodes = new_nodes

    current_node, destination = "AAA", "ZZZ"
    iterator = itertools.cycle(directions)
    steps1 = 0
    while current_node != destination:
        current_node = nodes[current_node][0 if next(iterator) == "L" else 1]
        steps1 += 1

    # current_nodes = []
    # for node in nodes:
    #     if node[-1] == "A":
    #         current_nodes.append(node)
    # iterator = itertools.cycle(directions)
    # steps2 = 0
    # while True:
    #     direction = next(iterator)
    #     for i, current_node in enumerate(current_nodes):
    #         current_nodes[i] = nodes[current_node][0 if direction == "L" else 1]
    #     steps2 += 1
    #     if all(current_node[-1] == "Z" for current_node in current_nodes):
    #         break

    print(f"Part 1: {steps1}")
    # print(f"Part 2: {steps2}")

day8("Day8/day8.txt")