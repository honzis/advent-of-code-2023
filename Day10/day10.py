def main(file):
    with open(file, "r") as f:
        rows = f.read().splitlines()

    right, left, up, down = ((0, 1), (0, -1), (-1, 0), (1, 0))

    pipe_directions = {
        "|": {1: (down, down), 2: (up, up)},
        "-": {1: (right, right), 2: (left, left)},
        "L": {1: (down, right), 2: (left, up)},
        "J": {1: (right, up), 2: (down, left)},
        "7": {1: (up, left), 2: (right, down)},
        "F": {1: (up, right), 2: (left, down)}
    }

    def merge(l1, l2):
        return [sum(l) for l in zip(l1, l2)]

    def go_next(current_pos, pipe_pos, rows):
        pipe = rows[pipe_pos[0]][pipe_pos[1]]

        if pipe in ("|", "L"):
            if current_pos[0] < pipe_pos[0]:
                n = 1
            else:
                n = 2
        elif pipe in ("-", "J"):
            if current_pos[1] < pipe_pos[1]:
                n = 1
            else:
                n = 2
        elif pipe in ("7", "F"):
            if current_pos[1] == pipe_pos[1]:
                n = 1
            else:
                n = 2
        else:
            raise RuntimeError
        
        directions = pipe_directions[pipe][n]
        current_pos = merge(current_pos, directions[0])
        pipe_pos = merge(pipe_pos, directions[1])

        return (current_pos, pipe_pos)
    
    current_pos = None
    for i, row in enumerate(rows):
        for j, tile in enumerate(row):
            if tile == "S":
                current_pos = [i, j]
                break
        if not current_pos is None:
            break
    
    pipe_pos = None
    for i in range(current_pos[0] - 1, current_pos[0] + 2):
        for j in range(current_pos[1] - 1, current_pos[1] + 2):
            if rows[i][j] not in ("|", "-", "L", "J", "7", "F") or i < 0 or j < 0 or i >= len(rows) or j >= len(rows[0]) or i != current_pos[0] and j != current_pos[1]:
                continue
            pipe = rows[i][j]
            for direction in pipe_directions[pipe].values():
                if merge(direction[0], current_pos) == [i, j]:
                    pipe_pos = [i, j]
                    break
            if not pipe_pos is None:
                break
        if not pipe_pos is None:
            break
    
    pipe_count = 0
    while rows[pipe_pos[0]][pipe_pos[1]] != "S":
        current_pos, pipe_pos = go_next(current_pos, pipe_pos, rows)
        pipe_count += 1
    
    print(f"Part 1: {(pipe_count + 1) // 2}")

main("Day10/day10.txt")