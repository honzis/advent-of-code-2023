def day1(file, include_numbers):
    lines = []
    with open(file, "r") as f:
        for line in f:
            lines.append(line.replace("\n", ""))

    numbers = {"one": "1", "two": "2", "three": "3", "four": "4", "five": "5", "six": "6", "seven": "7", "eight": "8", "nine": "9"}
    calibration_values = []
    for line in lines:
        found1 = False
        found2 = False
        for i in range(len(line)):
            char1 = line[i]
            char2 = line[-i - 1]
            if char1.isnumeric() and not found1:
                first_digit = char1
                found1 = True
            if char2.isnumeric() and not found2:
                last_digit = char2
                found2 = True
            if include_numbers:
                for number in numbers.keys():
                    if number in line[:i + 1] and not found1:
                        first_digit = numbers[number]
                        found1 = True
                    if number in line[-i - 1:] and not found2:
                        last_digit = numbers[number]
                        found2 = True
                    if found1 and found2:
                        break
            if found1 and found2:
                break

        calibration_values.append(int(first_digit + last_digit))
    return sum(calibration_values)

print(f"Part 1: {day1('Day1/day1.txt', False)}")
print(f"Part 2: {day1('Day1/day1.txt', True)}")
