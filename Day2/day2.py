def day2(file):
    colors = ("red", "green", "blue")
    cubes_in_bag = {"red": 12, "green": 13, "blue": 14}
    game_ids = []
    powers = []

    with open(file, "r") as f:
        for line in f:
            possible = True
            game_id = int(line[4:line.index(":")])
            cubes_min = {"red": 0, "green": 0, "blue": 0}

            for set_ in line[line.index(":")+1:-1].split(";"):
                cubes_in_set = {"red": 0, "green": 0, "blue": 0}
                for cubes in set_[1:].split(", "):
                    value, color = cubes.split(" ")
                    cubes_in_set[color] = int(value)

                for color in colors:
                    if cubes_in_set[color] > cubes_in_bag[color]:
                        possible = False
                    if cubes_in_set[color] > cubes_min[color]:
                        cubes_min[color] = cubes_in_set[color]

            if possible:
                game_ids.append(game_id)

            power = 1
            for x in cubes_min.values():
                power *= x
            powers.append(power)

    print(f"Part 1: {sum(game_ids)}")
    print(f"Part 2: {sum(powers)}")

day2("Day2/day2.txt")
